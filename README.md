# highlight.js - an automated repository for Composer availability.

_Software is compiled in "full" mode, cover all languages._

<table>
    <thead>
            <tr>
                <th>param</th>
                <th>value</th>
            </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>URL git origin</b></td>
            <td id="origin"><a href="https://github.com/highlightjs/highlight.js/" title="highlight.js">https://github.com/highlightjs/highlight.js/</a></td>
        </tr>
        <tr>
            <td><b>URL git fredericpetit</b></td>
            <td id="repository"><a href="https://gitlab.com/fredericpetit/highlightjs-composer/" title="highlight.js">https://gitlab.com/fredericpetit/highlightjs-composer/</a></td>
        </tr>
        <tr>
            <td><b>NAMESPACE</b> (composer)</td>
            <td id="composer"><a href="https://packagist.org/packages/fredericpetit/highlightjs-composer/" title="highlight.js">highlightjs-composer</a></td>
        </tr>
        <tr>
            <td><b>RELEASE</b> (latest)</td>
            <td id="tag">11.11.1</td>
        </tr>
        <tr>
            <td><b>DATE</b> (commit)</td>
            <td id="date">jeu. 26 déc. 2024 11:31:10 CET</td>
        </tr>
        <tr>
            <td><b>SCHEDULE</b> (pipeline)</td>
            <td id="schedule">11h30 every Monday & Thursday.</td>
        </tr>
    </tbody>
</table>
