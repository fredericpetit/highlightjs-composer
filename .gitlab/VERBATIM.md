# VERBATIM.

`Model : 2023-04-13.`

## License.
1. choose license from [https://spdx.org/licenses/](https://spdx.org/licenses/).

## Tokens.
1. create private token 'GITLAB_TOKEN' with rights 'api, write_repository, write_registry'.
1. add CI/CD variables 'CUSTOM_EMAIL' (Protected, Masked, Expanded), 'CUSTOM_NAME' (Protected, Expanded) and 'GITLAB_TOKEN' (Protected, Masked, Expanded).

## CI/CD.
1. add _./.gitlab/default-ci.yml_ & _./.gitlab/extend-ci.yml_ parameters, based on '_Self_' home.
1. add _./gitlab-ci.yml_.

## Composer.
1. add packagist.org connection in '_parameters > integrations_'.

## Pipeline.
1. add a schedule  pipeline (1 check each monday at 11:30, 1 check each thursday at 11:30)
1. add trigger with "build" keyword in commit & web trigger action.

## Rights.
- adapt some visibility rights (web access releases for example).